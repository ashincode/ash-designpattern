package com.ash.b.structural.proxy.dynamics;

/**
 * 抽象角色(之前是抽象类，此处应改为接口)
 *
 * @author Ash
 */
public interface FirstSubject {

    void request();
}
