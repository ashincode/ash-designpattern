package com.ash.b.structural.proxy.simple;

/**
 * @author Ash
 */
public interface Subject {

    void request();

}
