package com.ash.b.structural.proxy.simple;

/**
 * 那么真实角色必须是事先已经存在的，并将其作为代理对象的内部属性。但是实际使用时，一个真实角色必须对应一个 代理角色，如果大量使用会导致类的急剧膨胀；此外，如果事先并不知道真实角色，该如何使用代理呢？ 这个问题可以通过Java的动态代理类来解决。
 *
 * @author Ash
 */
public class SubjectProxy implements Subject {

    private RealSubject realSubject;

    @Override
    public void request() {
        System.out.println("before:you can do something");

        if (realSubject == null) {
            realSubject = new RealSubject();
        }

        realSubject.request();

        System.out.println("after:you can do something");
    }

}
