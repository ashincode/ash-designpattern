package com.ash.b.structural.proxy.dynamics;

/**
 * 具体角色RealSubject
 *
 * @author Ash
 */
public class FirstRealSubject implements FirstSubject {

    @Override
    public void request() {
        System.out.println("I'm a first subject implement");
    }

}
