package com.ash.b.structural.proxy.dynamics;

public class SecondRealSubject implements SecondSubject {

    @Override
    public void request(String msg) {
        System.out.println("I'm a second real subject implement,say " + msg);
    }

}
