package com.ash.b.structural.proxy.spring;

import com.ash.b.structural.proxy.spring.advice.GreetingAroundAdvice;
import com.ash.b.structural.proxy.spring.service.Greeting;
import com.ash.b.structural.proxy.spring.service.impl.GreetingImpl;
import org.springframework.aop.framework.ProxyFactory;

/**
 * spring 编程式方式实现代理
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        // 创建代理工厂
        ProxyFactory proxyFactory = new ProxyFactory();
        // 射入目标类对象
        proxyFactory.setTarget(new GreetingImpl());
//        // 添加前置增强
//        proxyFactory.addAdvice(new GreetingBeforeAdvice());
//        // 添加后置增强
//        proxyFactory.addAdvice(new GreetingAfterAdvice());

        proxyFactory.addAdvice(new GreetingAroundAdvice());

        // 从代理工厂中获取代理
        Greeting greeting = (Greeting) proxyFactory.getProxy();
        // 调用代理的方法
        greeting.sayHello("Jack");
    }
}
