package com.ash.b.structural.proxy.spring.service.impl;

import com.ash.b.structural.proxy.spring.service.Greeting;

/**
 * @author Ash
 */
public class GreetingImpl implements Greeting {

    @Override
    public void sayHello(String name) {
        System.out.println("hello " + name);
    }
}
