package com.ash.b.structural.proxy.simple;

public class RealSubject implements Subject {

    @Override
    public void request() {
        System.out.println("我是concreteObject,上面和下面的行为是后面加的");
    }

}
