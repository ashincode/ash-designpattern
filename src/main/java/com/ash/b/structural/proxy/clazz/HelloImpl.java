package com.ash.b.structural.proxy.clazz;

import net.sf.cglib.proxy.Enhancer;

/**
 * 基于类的代理需要引入cglib包 <br>
 * <p>
 * 参考文章 https://www.jianshu.com/p/9a61af393e41?from=timeline&isappinstalled=0
 * 解决了 无接口，也能代理的问题
 * 通过callbackfilter，也能实现不同的方法，使用不同的代理问题，或者指定某个方法代理
 * <p>
 * 缺点：
 *
 * @author Ash
 */
public class HelloImpl {


    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(HelloImpl.class);
        enhancer.setCallback(new HelloImplEnhance());

        // 方式2
        HelloImpl impl = (HelloImpl) enhancer.create();

        // 方式2
        // HelloImpl impl = (HelloImpl) Enhancer.create(HelloImpl.class,new HelloImplEnhance());
        impl.say();

        impl.sayProtected();

        impl.sayPrivate();

        HelloImpl.staticMethod();
    }

    public static void staticMethod() {
        System.out.println("public static void staticMethod()...");
    }

    public void say() {
        System.out.println("public void say()...");
    }

    protected void sayProtected() {
        System.out.println("protected void sayProtected()...");
    }

    private void sayPrivate() {
        System.out.println("private void sayPrivate...");
    }

}
