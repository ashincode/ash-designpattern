package com.ash.b.structural.proxy.simple;

/**
 * 定义:为其他对象提供一种代理以控制对这个对象的访问。 静态代理<br>
 * 缺陷:这样写没错，但是有个问题，XxxProxy 这样的类会越来越多，如何才能将这些代理类尽可能减少呢？最好只有一个代理类。
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        SubjectProxy proxy = new SubjectProxy();
        proxy.request();
    }

}
