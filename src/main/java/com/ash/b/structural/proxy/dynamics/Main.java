package com.ash.b.structural.proxy.dynamics;

import com.ash.b.structural.proxy.clazz.HelloImpl;

/**
 * jdk的动态代理只能基于接口<br>
 * 如果需要基于类的动态代理需要使用cglib包<br>
 * 动态代理，减少了静态代理 xxProxy的编写<br>
 * 这是aop的真实面目，理解了这个，对aop的理解就很容易了<br>
 * 基于接口的动态代理
 * <p>
 * 缺点：
 * 必须要要接口才能代理
 * 代理的所有的接口方法，无法个性化定制代理某个方法
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) throws Throwable {
        FirstSubject firstSubject = new FirstRealSubject();

        // 以下是一次性生成代理
        FirstSubject firstSubjectProxy = DynamicSubjectProxy.newProxy(firstSubject);
        firstSubjectProxy.request();

        System.out.println(firstSubjectProxy.getClass().getName());

        System.out.println("================================");

        SecondSubject secondSubject = new SecondRealSubject();

        // 以下是一次性生成代理
        SecondSubject secondSubjectProxy = DynamicSubjectProxy.newProxy(secondSubject);
        secondSubjectProxy.request("Hello World");

        System.out.println(secondSubjectProxy.getClass().getName());

        // 基于类的代理 这里是败笔的需要用cglib
        HelloImpl hello = DynamicSubjectProxy.newProxy(new HelloImpl());
        hello.say();

    }
}