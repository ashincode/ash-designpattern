package com.ash.b.structural.proxy.spring.service;

/**
 * @author Ash
 */
public interface Greeting {

    void sayHello(String name);
}
