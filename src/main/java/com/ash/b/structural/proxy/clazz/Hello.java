package com.ash.b.structural.proxy.clazz;

/**
 * 基于类的代理需要引入cglib包 <br>
 * <p>
 * 参考文章 https://www.jianshu.com/p/9a61af393e41?from=timeline&isappinstalled=0
 * 解决了 无接口，也能代理的问题
 *
 * @author Ash
 */
public interface Hello {


    void say();

}
