package com.ash.b.structural.bridge.coffee;

import com.ash.b.structural.bridge.behave.CoffeeBehavier;

/**
 * 大杯
 *
 * @author Ash
 */
public class SuperSizeCoffee extends Coffee {

    public SuperSizeCoffee(CoffeeBehavier coffeeBehavier) {
        super(coffeeBehavier);
    }

    @Override
    public void pourCoffee() {
        System.out.print("大杯,");
        CoffeeBehavier coffeeBehavier = this.getCoffeeBehavier();
        coffeeBehavier.pourSomething();
    }
}
