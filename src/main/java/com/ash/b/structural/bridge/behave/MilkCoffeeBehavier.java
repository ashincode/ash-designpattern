package com.ash.b.structural.bridge.behave;

/**
 * 加奶
 *
 * @author Ash
 */
public class MilkCoffeeBehavier implements CoffeeBehavier {

    public MilkCoffeeBehavier() {
    }

    @Override
    public void pourSomething() {
        System.out.println("加了美味的牛奶");
    }
}