package com.ash.b.structural.bridge.coffee;

import com.ash.b.structural.bridge.behave.CoffeeBehavier;

public abstract class Coffee {

    private CoffeeBehavier coffeeBehavier;

    public Coffee(CoffeeBehavier coffeeBehavier) {
        this.coffeeBehavier = coffeeBehavier;
    }

    public CoffeeBehavier getCoffeeBehavier() {
        return this.coffeeBehavier;
    }

    public abstract void pourCoffee();
}
