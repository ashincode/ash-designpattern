package com.ash.b.structural.bridge.behave;

/**
 * 什么都没加
 *
 * @author Ash
 */
public class FragrantCoffeeBehavier implements CoffeeBehavier {

    public FragrantCoffeeBehavier() {
    }

    @Override
    public void pourSomething() {
        System.out.println("什么也没加,清香");
    }
}