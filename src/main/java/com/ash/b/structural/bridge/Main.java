package com.ash.b.structural.bridge;

import com.ash.b.structural.bridge.behave.FragrantCoffeeBehavier;
import com.ash.b.structural.bridge.coffee.MediumCoffee;
import com.ash.b.structural.bridge.coffee.SuperSizeCoffee;
import com.ash.b.structural.bridge.behave.MilkCoffeeBehavier;
import com.ash.b.structural.bridge.coffee.Coffee;

/**
 * 桥接模式 定义:将抽象部分与它的实现部分分离，使它们都可以独立地变化
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        // 中杯加奶
        Coffee mediumCoffee = new MediumCoffee(new MilkCoffeeBehavier());
        mediumCoffee.pourCoffee();
        // 中杯不加奶
        Coffee mediumCoffee2 = new MediumCoffee(new FragrantCoffeeBehavier());
        mediumCoffee2.pourCoffee();

        // 大杯加奶
        Coffee superSizeCoffee = new SuperSizeCoffee(new MilkCoffeeBehavier());
        superSizeCoffee.pourCoffee();

        // 大杯不加奶
        Coffee superSizeCoffee2 = new SuperSizeCoffee(new FragrantCoffeeBehavier());
        superSizeCoffee2.pourCoffee();

    }

}
