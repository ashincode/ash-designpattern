package com.ash.b.structural.bridge.coffee;

import com.ash.b.structural.bridge.behave.CoffeeBehavier;

/**
 * 中杯
 *
 * @author Ash
 */
public class MediumCoffee extends Coffee {

    public MediumCoffee(CoffeeBehavier coffeeBehavier) {
        super(coffeeBehavier);
    }

    @Override
    public void pourCoffee() {
        System.out.print("中杯,");
        CoffeeBehavier coffeeBehavier = this.getCoffeeBehavier();
        coffeeBehavier.pourSomething();
    }
}
