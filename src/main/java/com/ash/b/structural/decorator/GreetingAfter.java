package com.ash.b.structural.decorator;

/**
 * @author Ash
 */
public class GreetingAfter extends GreetingDecorator {

    public GreetingAfter(Greeting greeting) {
        super(greeting);
    }

    @Override
    public void say() {
        super.say();

        after();
    }

    private void after() {
        System.out.println("After");
    }
}
