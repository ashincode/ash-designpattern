package com.ash.b.structural.decorator;

/**
 * @author Ash
 */
public interface Greeting {

    void say();
}
