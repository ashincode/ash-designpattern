package com.ash.b.structural.decorator;

/**
 * 装饰者模式 动态的给一个对象增加一些额外的职责
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        Greeting greeting = new GreetingBefore(new GreetingAfter(new GreetingImpl()));
        greeting.say();
    }

}
