package com.ash.b.structural.decorator;

/**
 * @author Ash
 */
public class GreetingImpl implements Greeting {

    @Override
    public void say() {
        System.out.println("say hello");
    }
}
