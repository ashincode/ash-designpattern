package com.ash.b.structural.decorator;

/**
 * @author Ash
 */
public abstract class GreetingDecorator implements Greeting {

    private Greeting greeting;

    public GreetingDecorator(Greeting greeting) {
        this.greeting = greeting;
    }

    @Override
    public void say() {
        greeting.say();
    }
}
