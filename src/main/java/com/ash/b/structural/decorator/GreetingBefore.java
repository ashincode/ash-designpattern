package com.ash.b.structural.decorator;

/**
 * @author Ash
 */
public class GreetingBefore extends GreetingDecorator {

    public GreetingBefore(Greeting greeting) {
        super(greeting);
    }

    @Override
    public void say() {
        before();
        super.say();
    }

    private void before() {
        System.out.println("Before");
    }
}
