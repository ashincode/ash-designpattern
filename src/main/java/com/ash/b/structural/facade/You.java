package com.ash.b.structural.facade;

/***
 * 外观模式
 * 定义:为子系统的一组接口提供一个一致的界面，Facade模式定义了一个高层接口,这个接口使得这一个子系统更加容易使用
 *
 * @author Ash
 *
 */
public class You {

    public static void main(String[] args) {
        Computer facade = new Computer();
        facade.startComputer();
    }
}

/* Complex parts */
class CPU {
    public void freeze() {
    }

    public void jump(long position) {
    }

    public void execute() {
    }
}

class Memory {
    public void load(long position, byte[] data) {
    }
}

class HardDrive {
    public byte[] read(long lba, int size) {
        return null;
    }
}

/* Facade */
/*
 * 可以把 this.cpu = new CPU(); this.memory = new Memory(); this.hardDrive = new HardDrive(); 放到public void
 * startComputer()的内部吗？？
 */
class Computer {

    private CPU cpu;
    private Memory memory;
    private HardDrive hardDrive;

    public Computer() {
        this.cpu = new CPU();
        this.memory = new Memory();
        this.hardDrive = new HardDrive();
    }

    public void startComputer() {
        cpu.freeze();
        memory.load(0x16f, hardDrive.read(0x12, 15));
        cpu.jump(0x45);
        cpu.execute();
    }
}
