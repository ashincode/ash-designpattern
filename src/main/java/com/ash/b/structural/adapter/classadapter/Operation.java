package com.ash.b.structural.adapter.classadapter;

public interface Operation {

    int add(int i, int j);

}
