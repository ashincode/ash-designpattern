package com.ash.b.structural.adapter.objectadapter;

public interface Operation {

    int add(int i, int j);

    int minus(int i, int j);

    int multiply(int i, int j);
}
