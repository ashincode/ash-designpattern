package com.ash.b.structural.adapter.objectadapter;

import com.ash.b.structural.adapter.thirdlib.OtherAdd;
import com.ash.b.structural.adapter.thirdlib.OtherMinus;
import com.ash.b.structural.adapter.thirdlib.OtherMultiply;

public class OperationImpl implements Operation {

    private OtherAdd add;
    private OtherMinus minus;
    private OtherMultiply multiply;

    public void setAdd(OtherAdd add) {
        this.add = add;
    }

    public void setMinus(OtherMinus minus) {
        this.minus = minus;
    }

    public void setMultiply(OtherMultiply multiplied) {
        this.multiply = multiplied;
    }

    @Override
    public int add(int i, int j) {
        return add.otherAdd(i, j);
    }

    @Override
    public int minus(int i, int j) {
        return minus.otherMinus(i, j);
    }

    @Override
    public int multiply(int i, int j) {
        return multiply.otherMultiply(i, j);
    }

}
