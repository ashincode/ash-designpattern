package com.ash.b.structural.adapter.classadapter;

/**
 * 定义:将一个类的接口转换成客户希望的另外一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作<br>
 * 类适配
 *
 * @author Ash
 */

public class Main {

    /**
     * 客户的开发人员定义了一个接口，期望用这个接口来完成整数的求和操作 Operation 开发人员在了解这个接口的定义后，发现一个第三方类OtherOperation,里面有一个方法能实现他们期望的功能 以上第三方类OtherOperation的方法public int otherAdd(int a,int
     * c)所提供的功能,完全能符合客户的期望,所以只需要想办法把OtherOperation的otherAdd(int a,int c)和客户的Operation接口联系起来 ,让这个第三方类来为客户提供他们期望的服务就行了，这样就避免了开发人员再度去研究类似OtherOperation的otherAdd(int a,int
     * c)方法的实现（利用已有的轮子，避免重复发明），这方法之一，就是用适配器模式： 以上就是适配器的实现方法之一，类适配器，在以上实现中存在着三中角色分别是： 1：适配目标角色：Operation。 2：适配类（原）角色：OtherOperation。 3：适配器角色：AdapterOperation。 其中适配器角色是适配器模式的核心。
     */
    public static void main(String[] args) {
        Operation instance = new OperationImpl();
        System.out.println(instance.add(2, 2));
    }

}
