package com.ash.b.structural.adapter.thirdlib;

/**
 * 作为第三类库
 *
 * @author Ash
 */
public class OtherAdd {

    public int otherAdd(int i, int j) {
        return i + j;
    }

}
