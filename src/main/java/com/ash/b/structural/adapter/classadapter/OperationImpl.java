package com.ash.b.structural.adapter.classadapter;

import com.ash.b.structural.adapter.thirdlib.OtherAdd;

/**
 * 通过扩展不兼容的类，使得OtherAdd类可以和Operation接口一起工作
 *
 * @author Ash
 */
public class OperationImpl extends OtherAdd implements Operation {

    @Override
    public int add(int i, int j) {
        return otherAdd(i, j);
    }

}
