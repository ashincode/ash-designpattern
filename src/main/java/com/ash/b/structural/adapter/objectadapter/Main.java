package com.ash.b.structural.adapter.objectadapter;

import com.ash.b.structural.adapter.thirdlib.OtherAdd;
import com.ash.b.structural.adapter.thirdlib.OtherMinus;
import com.ash.b.structural.adapter.thirdlib.OtherMultiply;

/**
 * 定义:将一个类的接口转换成客户希望的另外一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作<br>
 * 对象适配
 * 使用场景：@see Arrays.asList 体现的是适配器模式
 *
 * @author Ash
 */

public class Main {

    /**
     * 对象适配 适配器并不是通过继承来获取适配类（原）的功能的，而是通过适配类的对象来获取的，
     * 这就解决了java不能多继承所带来的不便了。这也是java提倡的编程思想之一 ，即尽量使用聚合不要使用继承。
     */
    public static void main(String[] args) {
        OperationImpl instance = new OperationImpl();
        instance.setAdd(new OtherAdd());
        instance.setMinus(new OtherMinus());
        instance.setMultiply(new OtherMultiply());

        System.out.println(instance.add(2, 2));
        System.out.println(instance.minus(2, 2));
        System.out.println(instance.multiply(3, 2));
    }

}
