package com.ash.b.structural.flyweight;

/**
 * @Author: Ash
 * @Date: 2020/10/22
 * @Description: com.Ash.structual.flyweight
 * @Version: 1.0.0
 */
class CoffeeOrderContext {

    private int tableNumber;

    public CoffeeOrderContext(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public int getTable() {
        return this.tableNumber;
    }
}