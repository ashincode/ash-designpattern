package com.ash.b.structural.flyweight;

/**
 * ConcreteFlyweight object that creates ConcreteFlyweight
 *
 * @Author: Ash
 * @Date: 2020/10/22
 * @Description: com.Ash.structual.flyweight
 * @Version: 1.0.0
 */

class CoffeeFlavor implements CoffeeOrder {

    private String flavor;

    public CoffeeFlavor(String newFlavor) {
        this.flavor = newFlavor;
    }


    @Override
    public void serveCoffee(CoffeeOrderContext context) {
        System.out.println("Serving Coffee flavor " + flavor + " to table number " + context.getTable());
    }

    public String getFlavor() {
        return this.flavor;
    }
}
