package com.ash.b.structural.flyweight;

/**
 * Flyweight object interface
 *
 * @author Ash
 */
interface CoffeeOrder {

    void serveCoffee(CoffeeOrderContext context);

}
