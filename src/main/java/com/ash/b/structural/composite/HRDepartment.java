package com.ash.b.structural.composite;

/***
 *
 * @author Ash
 *
 */
public class HRDepartment extends Company {

    public HRDepartment(String name) {
        super(name);
    }

    public HRDepartment() {
        super();
    }

    @Override
    public void add(Company company) {

    }

    @Override
    public void remove(Company company) {

    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void display(int depth) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < depth; i++) {
            sb.append("-");
        }
        System.out.println(new String(sb) + this.getName());
    }

}
