package com.ash.c.behavioral.interpreter;

/**
 * @author Ash
 */
public class MultiplyNonterminalExpression extends AExpression {

    private AExpression left;
    private AExpression right;

    public MultiplyNonterminalExpression(AExpression left, AExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int interpreter(Context context) {
        return this.left.interpreter(context) * this.right.interpreter(context);
    }
}
