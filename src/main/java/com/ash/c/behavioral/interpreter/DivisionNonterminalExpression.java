package com.ash.c.behavioral.interpreter;

/**
 * @author Ash
 */
public class DivisionNonterminalExpression extends AExpression {
    private final AExpression left;
    private final AExpression right;

    public DivisionNonterminalExpression(AExpression left, AExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int interpreter(final Context context) {
        final int value = this.right.interpreter(context);
        if (value != 0) {
            return this.left.interpreter(context) / value;
        }
        return -1111;
    }

}
