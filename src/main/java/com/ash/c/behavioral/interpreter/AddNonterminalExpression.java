package com.ash.c.behavioral.interpreter;

/**
 * @author Ash
 */
public class AddNonterminalExpression extends AExpression {

    private final AExpression left;
    private final AExpression right;

    public AddNonterminalExpression(final AExpression left, final AExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int interpreter(final Context context) {
        return this.left.interpreter(context) + this.right.interpreter(context);
    }
}
