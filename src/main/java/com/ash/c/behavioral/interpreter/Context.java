package com.ash.c.behavioral.interpreter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ash
 */
public class Context {

    private final Map<String, Integer> valueMap = new HashMap<>();

    public void addValue(String key, int value) {
        valueMap.put(key, Integer.valueOf(value));
    }

    public int getValue(String key) {
        return valueMap.get(key).intValue();
    }

}
