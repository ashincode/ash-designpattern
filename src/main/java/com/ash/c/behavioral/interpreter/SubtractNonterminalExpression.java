package com.ash.c.behavioral.interpreter;

/**
 * @author Ash
 */
public class SubtractNonterminalExpression extends AExpression {

    private final AExpression left;
    private final AExpression right;

    public SubtractNonterminalExpression(AExpression left, AExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int interpreter(Context context) {
        return this.left.interpreter(context) - this.right.interpreter(context);
    }

}
