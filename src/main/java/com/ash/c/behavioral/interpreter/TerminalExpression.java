package com.ash.c.behavioral.interpreter;

/**
 * @author Ash
 */
public class TerminalExpression extends AExpression {

    private final int i;

    public TerminalExpression(int i) {
        this.i = i;
    }

    @Override
    public int interpreter(Context context) {
        return this.i;
    }
}
