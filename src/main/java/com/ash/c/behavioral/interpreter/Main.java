package com.ash.c.behavioral.interpreter;

/**
 * 定义：给定一种语言，定义他的文法的一种表示，并定义一个解释器，该解释器使用该方法来解释语言中的句子
 *
 * @author Ash
 */
public class Main {

    /**
     * (a*c)/(a-c+2)
     *
     * @param args
     */
    public static void main(final String[] args) {
        final Context context = new Context();
        context.addValue("a", 7);
        context.addValue("c", 8);
        context.addValue("b", 2);

        MultiplyNonterminalExpression multiplyValue = new MultiplyNonterminalExpression(new TerminalExpression(context.getValue("a")), new TerminalExpression(context.getValue("c")));

        SubtractNonterminalExpression subtractValue = new SubtractNonterminalExpression(new TerminalExpression(context.getValue("a")), new TerminalExpression(context.getValue("c")));

        AddNonterminalExpression addValue = new AddNonterminalExpression(subtractValue, new TerminalExpression(context.getValue("b")));

        DivisionNonterminalExpression divisionValue = new DivisionNonterminalExpression(multiplyValue, addValue);

        System.out.println(divisionValue.interpreter(context));
    }
}
