package com.ash.c.behavioral.interpreter;

/**
 * @author Ash
 */
public abstract class AExpression {

    public abstract int interpreter(Context context);
}
