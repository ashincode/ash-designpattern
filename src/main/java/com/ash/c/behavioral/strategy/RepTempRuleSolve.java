package com.ash.c.behavioral.strategy;

public class RepTempRuleSolve {

    private RepTempStrategy strategy;

    public RepTempRuleSolve(RepTempStrategy rule) {
        this.strategy = rule;
    }

    public String getNewContext(String oldString) {
        strategy.setOldString(oldString);
        strategy.replace();

        return strategy.getNewString();
    }

    public void changeAlgorithm(RepTempStrategy newAlgorithm) {
        strategy = newAlgorithm;
    }

}
