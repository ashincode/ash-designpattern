package com.ash.c.behavioral.strategy;

public abstract class RepTempStrategy {

    protected String newString = "";

    protected String oldString = "";

    public void setOldString(String oldString) {
        this.oldString = oldString;
    }

    public String getNewString() {
        return newString;
    }

    public abstract void replace();

}
