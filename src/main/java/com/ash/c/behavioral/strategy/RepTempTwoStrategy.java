package com.ash.c.behavioral.strategy;

public class RepTempTwoStrategy extends RepTempStrategy {

    @Override
    public void replace() {
        newString = oldString.replaceFirst("aaa", "bbb");
        System.out.println("this is replace two");
    }

}
