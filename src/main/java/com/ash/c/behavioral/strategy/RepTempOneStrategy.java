package com.ash.c.behavioral.strategy;

public class RepTempOneStrategy extends RepTempStrategy {

    @Override
    public void replace() {
        newString = oldString.replaceFirst("aaa", "ccc");
        System.out.println("this is replace one");
    }

}
