package com.ash.c.behavioral.strategy;

/**
 * 定义：定义一组算法，将每一个算法都封装起来，并且使他们之间可以互换
 * <p>
 * 使用案例
 * <p>
 * Thread中的Runnable
 *
 * @author Ash
 */
public class Main {


    public static void main(String[] args) {
        RepTempRuleSolve solve = new RepTempRuleSolve(new RepTempOneStrategy());

        System.out.println(solve.getNewContext("aaaaddaadad"));

        solve.changeAlgorithm(new RepTempTwoStrategy());

        System.out.println(solve.getNewContext("aaaaddaadad"));

    }

}
