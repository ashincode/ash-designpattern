package com.ash.c.behavioral.iterator;

public interface Iterator<E> {

    E next();

    boolean hasNext();

}
