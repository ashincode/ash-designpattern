package com.ash.c.behavioral.iterator;

public interface Aggregate<E> {

    void add(E e);

    void remove(E e);

    Iterator<E> iterator();

}
