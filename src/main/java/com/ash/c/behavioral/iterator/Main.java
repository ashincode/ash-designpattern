package com.ash.c.behavioral.iterator;

/**
 * 定义：提供一种方法访问一个容器对象中各个元素，而又不暴露该对象的内部细节
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        Aggregate<String> ag = new ConcreteAggregate<>();
        ag.add("1");
        ag.add("12");
        ag.add("123");
        ag.add("1234");

        Iterator<String> it = ag.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
