package com.ash.c.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

public class ConcreteIterator<E> implements Iterator<E> {

    private List<E> list = new ArrayList<E>();
    private int cursor = 0;

    public ConcreteIterator(List<E> list) {
        this.list = list;
    }

    @Override
    public E next() {
        if (this.hasNext()) {
            return this.list.get(cursor++);
        }

        return null;
    }

    @Override
    public boolean hasNext() {
        return cursor != list.size();
    }

}
