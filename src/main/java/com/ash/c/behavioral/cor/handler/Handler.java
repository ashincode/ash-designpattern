package com.ash.c.behavioral.cor.handler;

import com.ash.c.behavioral.cor.request.Request;

/**
 * @author Ash
 */
public interface Handler {

    void handleRequest(Request request);

}
