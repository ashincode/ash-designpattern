package com.ash.c.behavioral.cor;

import com.ash.c.behavioral.cor.request.SaveRequest;
import com.ash.c.behavioral.cor.handler.Handler;
import com.ash.c.behavioral.cor.handler.HelpHandler;
import com.ash.c.behavioral.cor.handler.PrintHandler;
import com.ash.c.behavioral.cor.handler.SaveHandler;
import com.ash.c.behavioral.cor.request.HelpRequest;
import com.ash.c.behavioral.cor.request.PrintRequest;

/**
 * 定义：使多个对象都有机会处理请求，从而避免了请求的发送者和接收者之间的耦合关系。将这些对象连成一条链，并沿着这条链传递该请求，直到有对象处理它为止。
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        Handler handler1 = new HelpHandler(null);
        Handler handler2 = new PrintHandler(handler1);
        Handler handler3 = new SaveHandler(handler2);

        handler1.handleRequest(new HelpRequest());
        handler2.handleRequest(new PrintRequest());
        handler3.handleRequest(new SaveRequest());
    }

}
