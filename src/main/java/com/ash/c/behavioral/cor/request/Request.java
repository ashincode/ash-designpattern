package com.ash.c.behavioral.cor.request;

/**
 * @author Ash  on 2015/8/8.
 */
public interface Request {

    void doSomething();
}
