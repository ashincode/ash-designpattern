package com.ash.c.behavioral.cor.request;

/**
 * @author Ash  on 2015/8/8.
 */
public class PrintRequest implements Request {

    @Override
    public void doSomething() {
        System.out.print("打印请求");
    }
}
