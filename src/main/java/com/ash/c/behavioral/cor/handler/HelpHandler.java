package com.ash.c.behavioral.cor.handler;

import com.ash.c.behavioral.cor.request.HelpRequest;
import com.ash.c.behavioral.cor.request.Request;

/**
 * @author Ash
 */
public class HelpHandler implements Handler {

    // 后继处理器
    private Handler successor;

    public HelpHandler(Handler successor) {
        this.successor = successor;
    }

    @Override
    public void handleRequest(Request request) {
        if (request instanceof HelpRequest) {
            System.out.println("HelpHandler handle " + request.getClass().getSimpleName());

        } else {
            System.out.println("PrintHandler can't handle " + request.getClass().getSimpleName());
            if (successor != null) {
                successor.handleRequest(request);
            }
        }
    }
}
