package com.ash.c.behavioral.cor.request;

/**
 * @author Ash  on 2015/8/8.
 */
public class HelpRequest implements Request {

    @Override
    public void doSomething() {
        System.out.print("求助请求");
    }
}
