package com.ash.c.behavioral.cor.handler;

import com.ash.c.behavioral.cor.request.PrintRequest;
import com.ash.c.behavioral.cor.request.Request;

/**
 * @author Ash
 */
public class PrintHandler implements Handler {

    // 后继处理器
    private Handler successor;

    public PrintHandler(Handler successor) {
        this.successor = successor;
    }

    @Override
    public void handleRequest(Request request) {
        if (request instanceof PrintRequest) {
            System.out.println("PrintHandler handle " + request.getClass().getSimpleName());

        } else {
            System.out.println("PrintHandler can't handle " + request.getClass().getSimpleName());
            if (successor != null) {
                successor.handleRequest(request);
            }
        }
    }
}
