package com.ash.c.behavioral.state;

/**
 * @author Ash
 */
public class BlueState implements State {

    @Override
    public void handlePush(Context context) {
        // 如果是blue状态的切换到red
        context.setState(new RedState());
    }

    @Override
    public void handlePull(Context context) {
        context.setState(new GreenState());
    }

    @Override
    public String getStateName() {
        return "blue";
    }
}
