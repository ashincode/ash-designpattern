package com.ash.c.behavioral.state;

/**
 * @author Ash
 */
public class Context {

    private State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void push() {
        // 状态的切换的细节部分,在本例中是颜色的变化,已经封装在子类的handlepush中实现,这里无需关心
        state.handlePush(this);

        // 根据状态的变化 处理 相应的的业务
        System.out.println("处理颜色:" + state.getStateName());
    }

    public void pull() {
        state.handlePull(this);

        // 根据状态的变化 处理 相应的的业务
        System.out.println("处理颜色:" + state.getStateName());
    }

}
