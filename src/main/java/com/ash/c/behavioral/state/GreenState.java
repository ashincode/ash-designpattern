package com.ash.c.behavioral.state;

/**
 * @author Ash
 */
public class GreenState implements State {

    @Override
    public void handlePush(Context context) {
        context.setState(new BlueState());
    }

    @Override
    public void handlePull(Context context) {
        context.setState(new RedState());
    }

    @Override
    public String getStateName() {
        return "green";
    }
}
