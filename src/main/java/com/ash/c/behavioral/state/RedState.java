package com.ash.c.behavioral.state;

/**
 * @author Ash
 */
public class RedState implements State {

    @Override
    public void handlePush(Context context) {
        context.setState(new GreenState());
    }

    @Override
    public void handlePull(Context context) {
        context.setState(new BlueState());
    }

    @Override
    public String getStateName() {
        return "red";
    }
}
