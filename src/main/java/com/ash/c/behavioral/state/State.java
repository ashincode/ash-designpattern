package com.ash.c.behavioral.state;

/**
 * @author Ash
 */
public interface State {

    void handlePush(Context context);

    void handlePull(Context context);

    String getStateName();
}
