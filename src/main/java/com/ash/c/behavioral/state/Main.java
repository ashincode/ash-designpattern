package com.ash.c.behavioral.state;

/**
 * 定义：不同的状态,不同的行为;或者说,每个状态有着相应的行为.
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        Context context = new Context();
        context.setState(new RedState());

        context.push();
        context.push();
        context.push();
        System.out.println("回退===");

        context.pull();
        context.pull();
        context.pull();

    }

}
