package com.ash.c.behavioral.command.simple;

/**
 * 请求者角色
 *
 * @author Ash
 */
public class Invoker {

    /**
     * 持有命令对象
     */
    private Command command = null;

    /**
     * 构造方法
     */
    public Invoker(Command command) {
        this.command = command;
    }

    /**
     * 行动方法
     */
    public void action() {

        command.execute();
    }
}