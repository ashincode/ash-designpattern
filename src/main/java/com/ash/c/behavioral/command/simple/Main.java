package com.ash.c.behavioral.command.simple;

/**
 * 定义:将一个请求封装成一个对象，从而让你使用不同的请求把客户端参数化，对请求排队或者记录请求日志，可以提供命令的撤销和恢复功能<br>
 * 精髓：把命令的调用者与执行者分开，使双方不必关心对方是如何操作的
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        // 创建接收者
        Receiver receiver = new Receiver();
        // 创建命令对象，设定它的接收者
        Command command = new ConcreteCommand(receiver);
        // 创建请求者，把命令对象设置进去
        Invoker invoker = new Invoker(command);
        // 执行方法
        invoker.action();
    }

}
