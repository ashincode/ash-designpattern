package com.ash.c.behavioral.command.audio;

/**
 * 抽象命令角色
 *
 * @author Ash
 */
public interface Command {
    /**
     * 执行方法
     */
    void execute();
}
