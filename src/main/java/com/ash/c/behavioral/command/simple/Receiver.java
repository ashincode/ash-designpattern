package com.ash.c.behavioral.command.simple;

/**
 * 接收者角色类
 *
 * @author Ash
 */
public class Receiver {

    /**
     * 真正执行命令相应的操作
     */
    public void action() {
        System.out.println("执行操作");
    }
}