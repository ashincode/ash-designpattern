package com.ash.c.behavioral.visitor.visitable;

import com.ash.c.behavioral.visitor.Visitor;

/**
 * @author Ash
 */
public interface Visitable {

    void accept(Visitor visitor);

}
