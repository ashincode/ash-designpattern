package com.ash.c.behavioral.visitor.visitable;

import com.ash.c.behavioral.visitor.Visitor;

/**
 * @author Ash
 */
public class StringElement implements Visitable {

    private String value;

    public StringElement(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitString(this);
    }
}
