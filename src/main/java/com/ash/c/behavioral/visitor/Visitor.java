package com.ash.c.behavioral.visitor;

import com.ash.c.behavioral.visitor.visitable.FloatElement;
import com.ash.c.behavioral.visitor.visitable.StringElement;

/**
 * @author Ash
 */
public interface Visitor {

    void visitString(StringElement string);

    void visitFloat(FloatElement f);

}
