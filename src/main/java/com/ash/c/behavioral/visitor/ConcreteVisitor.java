package com.ash.c.behavioral.visitor;

import com.ash.c.behavioral.visitor.visitable.FloatElement;
import com.ash.c.behavioral.visitor.visitable.StringElement;

/**
 * @author Ash
 */
public class ConcreteVisitor implements Visitor {

    @Override
    public void visitString(StringElement string) {
        System.out.println("'" + string.getValue() + "'");
    }

    @Override
    public void visitFloat(FloatElement f) {
        System.out.println(f.getValue().toString() + "f");
    }
}
