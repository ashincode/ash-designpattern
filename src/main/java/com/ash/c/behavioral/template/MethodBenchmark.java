package com.ash.c.behavioral.template;

public class MethodBenchmark extends Benchmark {

    /**
     * 真正定义benchmark内容
     */
    @Override
    public void benchmark() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("i=" + i);
        }
    }
}
