package com.ash.c.behavioral.template;

/**
 * 定义：定义一个操作中的算法的框架，而将一些步骤延迟到子类中，使得子类可以不改变算法的结构即可重定义概算法中的某些特定步骤
 * <p>
 * 使用场景
 * <p>
 * Thread run方法
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        Benchmark operation = new MethodBenchmark();
        long duration = operation.repeat(12);
        System.out.println("The operation took " + duration + " milliseconds");
    }
}
