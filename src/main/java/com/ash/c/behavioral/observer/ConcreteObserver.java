package com.ash.c.behavioral.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * 观察者
 *
 * @author Ash
 */
public class ConcreteObserver implements Observer {

    /**
     * 实现接到通知进行更新的接口
     */
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("接到通知！");
    }

}