package com.ash.c.behavioral.observer;

/**
 * 定义:定义对象间一种一对多的依赖关系,使得当每一个对象改变状态，则所有的依赖于他的对象都会得到通知并自动更新
 * <p>
 * 使用场景
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        // 被观察者
        ConcreteObservable observable = new ConcreteObservable();

        // 观察者
        ConcreteObserver observer1 = new ConcreteObserver();
        ConcreteObserver observer2 = new ConcreteObserver();
        ConcreteObserver observer3 = new ConcreteObserver();
        ConcreteObserver observer4 = new ConcreteObserver();

        // 动态添加观察者
        observable.addObserver(observer1);
        observable.addObserver(observer2);
        observable.addObserver(observer3);
        observable.addObserver(observer4);

        observable.logicMethod();
    }
}
