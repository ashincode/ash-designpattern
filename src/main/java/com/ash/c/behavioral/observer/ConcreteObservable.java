package com.ash.c.behavioral.observer;

import java.util.Observable;

/**
 * 可被观察
 *
 * @author Ash
 */
public class ConcreteObservable extends Observable {


    /**
     * 被观察者自己的逻辑，并且定义了对该操作需要进行通知
     */
    public void logicMethod() {
        // 逻辑处理
        super.setChanged();

        // 通知所有观察者
        this.notifyObservers();
    }
}