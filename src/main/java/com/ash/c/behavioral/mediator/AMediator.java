package com.ash.c.behavioral.mediator;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author Ash
 */
public abstract class AMediator {

    /**
     * 中介者肯定需要保持有若干同事的联系方式*
     */
    protected Map<String, AColleague> colleagues = new Hashtable<String, AColleague>();

    /**
     * 中介者可以动态地与某个同事建立联系
     *
     * @param name
     * @param c
     */
    public void addColleague(String name, AColleague c) {
        this.colleagues.put(name, c);
    }

    /**
     * 中介者也可以动态地撤销与某个同事的联系
     *
     * @param name
     */
    public void deleteColleague(String name) {
        this.colleagues.remove(name);
    }

    /**
     * 中介者必须具备在同事之间处理逻辑、分配任务、促进交流的操作
     *
     * @param colleagueName
     * @param methodName
     */
    public abstract void execute(String colleagueName, String methodName);

}
