package com.ash.c.behavioral.mediator;

/**
 * @author Ash
 */
public class Mediator extends AMediator {

    @Override
    public void execute(String colleagueName, String methodName) {
        // 各自做好分内事
        if ("self".equals(methodName)) {
            if ("ColleagueA".equals(colleagueName)) {
                ColleagueA colleague = (ColleagueA) super.colleagues.get("ColleagueA");
                colleague.self();

            } else {
                ColleagueB colleague = (ColleagueB) super.colleagues.get("ColleagueB");
                colleague.self();
            }

        } else {
            // 与其他同事合作
            if ("ColleagueA".equals(colleagueName)) {
                ColleagueA colleague = (ColleagueA) super.colleagues.get("ColleagueA");
                colleague.out();

            } else {
                ColleagueB colleague = (ColleagueB) super.colleagues.get("ColleagueB");
                colleague.out();
            }
        }

    }
}
