package com.ash.a.creational.abstractfactory.fruit;

public class TropicalFruit implements Fruit {

    private String name;

    public TropicalFruit(String name) {
        System.out.println("热带工厂为您创建了：热带水果－" + name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}