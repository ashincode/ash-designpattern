package com.ash.a.creational.abstractfactory;

import com.ash.a.creational.abstractfactory.fruit.NorthernFruit;
import com.ash.a.creational.abstractfactory.fruit.Fruit;
import com.ash.a.creational.abstractfactory.veggie.NorthernVeggie;
import com.ash.a.creational.abstractfactory.veggie.Veggie;

public class NorthernGardener implements Gardener {

    @Override
    public Fruit createFruit(String name) {
        return new NorthernFruit(name);
    }

    @Override
    public Veggie createVeggie(String name) {
        return new NorthernVeggie(name);
    }
}