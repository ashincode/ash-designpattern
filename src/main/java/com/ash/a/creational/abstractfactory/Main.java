package com.ash.a.creational.abstractfactory;

/**
 * 定义:为创建一组相关的或相互依赖的对象提供一个接口,而且无需指定他们的具体类。<br/>
 * <i>工厂方法的区别:工厂方法模式针对的是一个产品等级结构，而抽象的工厂则是针对多个产品等级结构</i><br>
 * <br>
 * 在编程中,通常一个产品结构，表现为一个接口或者抽象类，也就是说，工厂方法模式提供的所有产品都是衍生自同一个接口和抽象类， 而抽象工厂模式提供的产品则是衍生自不同的接口和抽象类
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {
        Gardener tg = new TropicalGardener();
        tg.createVeggie("热带菜叶");
        tg.createFruit("海南椰子");

        Gardener ng = new NorthernGardener();
        ng.createVeggie("东北甜菜");
        ng.createFruit("雪梨");
    }
}
