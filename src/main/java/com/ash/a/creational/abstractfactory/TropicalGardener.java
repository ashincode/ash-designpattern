package com.ash.a.creational.abstractfactory;

import com.ash.a.creational.abstractfactory.fruit.Fruit;
import com.ash.a.creational.abstractfactory.fruit.TropicalFruit;
import com.ash.a.creational.abstractfactory.veggie.Veggie;
import com.ash.a.creational.abstractfactory.veggie.TropicalVeggie;

public class TropicalGardener implements Gardener {

    @Override
    public Fruit createFruit(String name) {
        return new TropicalFruit(name);
    }

    @Override
    public Veggie createVeggie(String name) {
        return new TropicalVeggie(name);
    }
}
