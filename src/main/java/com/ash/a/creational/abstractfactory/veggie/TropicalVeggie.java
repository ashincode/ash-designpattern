package com.ash.a.creational.abstractfactory.veggie;

public class TropicalVeggie implements Veggie {

    private String name;

    public TropicalVeggie(String name) {
        System.out.println("热带工厂为您创建了：热带蔬菜－" + name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
