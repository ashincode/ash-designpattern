package com.ash.a.creational.abstractfactory;

import com.ash.a.creational.abstractfactory.fruit.Fruit;
import com.ash.a.creational.abstractfactory.veggie.Veggie;

/**
 * 工厂模式--抽象工厂模式--一般性模式（农场应用） ReadMe: 抽象工厂角色：工厂接口
 */
public interface Gardener {

    Fruit createFruit(String name);

    Veggie createVeggie(String name);

}