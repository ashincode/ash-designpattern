package com.ash.a.creational.builder.builder;

import com.ash.a.creational.builder.product.Computer;

public class GamingComputerBuilder implements ComputerBuilder {

    // 这里的Computer不一定要通过builder方式去构建，你也可以去new 然后去set
    private Computer.Builder builder;

    public GamingComputerBuilder() {
        builder = new Computer.Builder();
    }

    @Override
    public void reset() {
        builder = new Computer.Builder();
    }

    @Override
    public void setCpu() {
        builder.setCpu("i9");
    }

    @Override
    public void setMemory() {
        builder.setMemory("16GB");
    }

    @Override
    public void setHardDrive() {
        builder.setHardDrive("SSD 1TB");
    }

    @Override
    public Computer getResult() {
        return builder.build();
    }
}



