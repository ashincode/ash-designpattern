package com.ash.a.creational.builder.builder;

import com.ash.a.creational.builder.product.Computer;

public class OfficeComputerBuilder implements ComputerBuilder {

    private Computer.Builder builder;

    public OfficeComputerBuilder() {
        builder = new Computer.Builder();
    }

    @Override
    public void reset() {
        builder = new Computer.Builder();
    }

    @Override
    public void setCpu() {
        builder.setCpu("i5");
    }

    @Override
    public void setMemory() {
        builder.setMemory("8GB");
    }

    @Override
    public void setHardDrive() {
        builder.setHardDrive("HDD 1TB");
    }

    @Override
    public Computer getResult() {
        return builder.build();
    }
}