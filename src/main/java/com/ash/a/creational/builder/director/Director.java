package com.ash.a.creational.builder.director;

import com.ash.a.creational.builder.builder.ComputerBuilder;

/**
 * 指挥官，指挥builder如何构建，构建过程
 *
 * @author Ash
 */
public class Director {



    public void construct(ComputerBuilder builder) {
        builder.reset();
        builder.setCpu();
        builder.setMemory();
        builder.setHardDrive();
    }
}