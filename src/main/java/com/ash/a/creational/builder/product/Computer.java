package com.ash.a.creational.builder.product;

public class Computer {

    private String cpu;
    private String memory;
    private String hardDrive;

    public Computer(Builder builder) {
        this.cpu = builder.cpu;
        this.memory = builder.memory;
        this.hardDrive = builder.hardDrive;
    }

    public String getCpu() {
        return cpu;
    }

    public String getMemory() {
        return memory;
    }

    public String getHardDrive() {
        return hardDrive;
    }

    // 内部静态类 Builder
    public static class Builder {
        private String cpu;
        private String memory;
        private String hardDrive;

        public Builder setCpu(String cpu) {
            this.cpu = cpu;
            return this;
        }

        public Builder setMemory(String memory) {
            this.memory = memory;
            return this;
        }

        public Builder setHardDrive(String hardDrive) {
            this.hardDrive = hardDrive;
            return this;
        }

        public Computer build() {
            return new Computer(this);
        }
    }
}

