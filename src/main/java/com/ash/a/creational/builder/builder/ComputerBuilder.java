package com.ash.a.creational.builder.builder;

import com.ash.a.creational.builder.product.Computer;

public interface ComputerBuilder {

    void reset();
    void setCpu();
    void setMemory();
    void setHardDrive();
    Computer getResult();
}
