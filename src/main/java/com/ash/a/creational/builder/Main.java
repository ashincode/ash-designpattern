package com.ash.a.creational.builder;

import com.ash.a.creational.builder.builder.ComputerBuilder;
import com.ash.a.creational.builder.builder.GamingComputerBuilder;
import com.ash.a.creational.builder.builder.OfficeComputerBuilder;
import com.ash.a.creational.builder.director.Director;
import com.ash.a.creational.builder.product.Computer;


/**
 * 定义:将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示<br>
 * 建造者模式与工厂模式类似，他们都是建造者模式，适用场景很相似。一般来说，如果产品的建造很复杂，那么请用工厂模式；如果产品的建造更复杂，那么请用建造者模式
 *
 * @author Ash
 */
public class Main {
    public static void main(String[] args) {
        // director是构建过程
        Director director = new Director();

        // Builder 是表示
        ComputerBuilder gamingBuilder = new GamingComputerBuilder();
        ComputerBuilder officeBuilder = new OfficeComputerBuilder();


        // 同样一个director 可以构建 两个表示 构建和表示分离

        // 构建游戏电脑
        director.construct(gamingBuilder);
        Computer gamingComputer = gamingBuilder.getResult();
        System.out.println("Gaming Computer: " + gamingComputer.getCpu() + ", " + gamingComputer.getMemory() + ", " + gamingComputer.getHardDrive());

        // 构建办公电脑
        director.construct(officeBuilder);
        Computer officeComputer = officeBuilder.getResult();
        System.out.println("Office Computer: " + officeComputer.getCpu() + ", " + officeComputer.getMemory() + ", " + officeComputer.getHardDrive());
    }
}

