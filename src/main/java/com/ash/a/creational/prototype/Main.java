package com.ash.a.creational.prototype;

/**
 * 定义:用原型实例指定创建对象的种类,并通过拷贝这些原型创建新的对象。
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        Student student1 = new Student();

        Watch watch1=new Watch();
        watch1.setName("watch1");
        watch1.setColor("green");

        student1.setStudentAge(12);
        student1.setStudentName("zjh");
        student1.setStudentSex(false);
        student1.setWatch(watch1);
        System.out.println(student1);

        Student student2 = student1.clone();
        student2.setStudentAge(333);
        student2.getWatch().setName("watch2");
        System.out.println(student1);
        System.out.println(student2);

    }

}
