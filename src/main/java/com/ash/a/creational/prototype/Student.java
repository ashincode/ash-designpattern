package com.ash.a.creational.prototype;

/**
 * 原型模式定义: 用原型实例指定创建对象的种类,并且通过拷贝这些原型创建新的对象.
 * Prototype模式允许一个对象再创建另外一个可定制的对象，根本无需知道任何如何创建的细节,
 * 工作原理是:通过将一个原型对象传给那个要发动创建的对象， 这个要发动创建的对象通过请求原型对象拷贝它们自己来实施创建。
 * <p>
 * 注意事项：对象的clone方法默认是浅拷贝，若想实现深拷贝需要重写clone方法实现属性对象的拷贝。
 *
 * @author Ash
 */
public class Student extends Object implements Cloneable {

    private String studentName;
    private Boolean studentSex;
    private Integer studentAge;

    private Watch watch;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Boolean getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(Boolean studentSex) {
        this.studentSex = studentSex;
    }

    public Integer getStudentAge() {
        return studentAge;
    }

    public void setStudentAge(Integer studentAge) {
        this.studentAge = studentAge;
    }

    public Watch getWatch() {
        return watch;
    }

    public void setWatch(Watch watch) {
        this.watch = watch;
    }

    @Override
    protected Student clone() throws CloneNotSupportedException {
        Student student = (Student) super.clone();
        // 这里需要拷贝watch引用对象，不然是知识浅拷贝
        student.setWatch(getWatch().clone());


        return student;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentName='" + studentName + '\'' +
                ", studentSex=" + studentSex +
                ", studentAge=" + studentAge +
                ", watch=" + watch +
                '}';
    }
}
