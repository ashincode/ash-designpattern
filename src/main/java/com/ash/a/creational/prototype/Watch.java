package com.ash.a.creational.prototype;

public class Watch implements  Cloneable{

    private  String name;

    private String color;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    protected Watch clone() throws CloneNotSupportedException {
        return (Watch)super.clone();
    }

    @Override
    public String toString() {
        return "Watch{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
