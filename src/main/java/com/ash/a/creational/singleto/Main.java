package com.ash.a.creational.singleto;

/**
 * 确保一个类只有一个实例，而且自行实例化并向整个系统提供这个实例<br>
 * 实现单例的方式有很多种，但是一个类就有一个实例这个核心是不变的，不用过多的去纠结实现的方式，而是理解概念
 *
 * @author Ash
 */
public class Main {

    public static void main(String[] args) {

    }
}
