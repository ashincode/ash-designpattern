package com.ash.a.creational.singleto;

/**
 * 双重锁检查
 *
 * @author Ash
 */
public class Singleton3 {

    private static volatile Singleton3 instance;

    private Singleton3() {

    }

    public static Singleton3 getInstance() {
        if (instance == null) {
            synchronized (Singleton3.class) {
                if (instance == null) {
                    instance = new Singleton3();
                }
            }
        }

        return instance;
    }

}
