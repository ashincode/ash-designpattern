package com.ash.a.creational.singleto;

/**
 * 这个是单例模式中最简单的例子，是用静态变量的 ，通过类加载的时候，new一个实例 然后用一个静态方法返回该实例，以后每次调用该静态方法，都是返回加载时创建的实例
 *
 * @author Ash
 */
public class Singleton1 {

    private static final Singleton1 instance = new Singleton1();

    private Singleton1() {

    }

    public static Singleton1 getInstance() {
        return instance;
    }
}