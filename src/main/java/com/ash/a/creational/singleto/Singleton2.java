package com.ash.a.creational.singleto;

/**
 * 这个方法比上面有所改进，不用每次都进行生成对象，只是第一次使用时生成实例，提高了效率！ 这种形式也叫 lazy initialization <br/>
 * 注意到lazy initialization形式中的synchronized，这个synchronized很重要， 如果没有synchronized，<br/>
 * 那么使用getInstance()是有可能得到多个Singleton实例 为什么要加synchronized？ <br/>
 * 我是这样理解的，如果instance是为空的情况下 ，突然有在高并发的情况下，<br/>
 * 假设有1000个人同时调用了 getInstance这时就出现了很多人都在 instance = new Singleton2()的情况。这时堆内存会的使用率会突然激增
 *
 * @author Ash
 */
public class Singleton2 {

    private static Singleton2 instance;

    private Singleton2() {

    }

    public static synchronized Singleton2 getInstance() {
        if (instance == null) {
            instance = new Singleton2();
        }

        return instance;
    }

}
