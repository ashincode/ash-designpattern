package com.ash.a.creational.factorymethod.factory;

import com.ash.a.creational.factorymethod.product.Apple;
import com.ash.a.creational.factorymethod.product.IPhone;

public class AppleFactory implements IPhoneFactory {

    @Override
    public IPhone createProduct() {
        return new Apple();
    }

}
