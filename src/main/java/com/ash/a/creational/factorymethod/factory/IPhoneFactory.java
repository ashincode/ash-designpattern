package com.ash.a.creational.factorymethod.factory;

import com.ash.a.creational.factorymethod.product.IPhone;

public interface IPhoneFactory {

    IPhone createProduct();

}
