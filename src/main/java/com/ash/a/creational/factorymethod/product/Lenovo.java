package com.ash.a.creational.factorymethod.product;

public class Lenovo implements IPhone {

    @Override
    public void sendMessage() {
        System.out.println("我是联想手机");

    }

    @Override
    public String receivedMessage() {
        return "我用联想手机收到消息了";
    }

}
