package com.ash.a.creational.factorymethod.product;

public interface IPhone {

    void sendMessage();

    String receivedMessage();

}
