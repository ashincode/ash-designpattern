package com.ash.a.creational.factorymethod.factory;

import com.ash.a.creational.factorymethod.product.IPhone;
import com.ash.a.creational.factorymethod.product.Lenovo;

public class LenovoFactory implements IPhoneFactory {

    @Override
    public IPhone createProduct() {
        return new Lenovo();
    }

}
