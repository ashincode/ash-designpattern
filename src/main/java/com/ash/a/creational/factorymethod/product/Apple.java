package com.ash.a.creational.factorymethod.product;

public class Apple implements IPhone {

    @Override
    public void sendMessage() {
        System.out.println("我是苹果手机");

    }

    @Override
    public String receivedMessage() {
        return "我用苹果手机收到消息了";
    }

}
